# Todo-App

## Target

มี input ในการกรอกข้อมูลและให้กดปุ่มเพื่อเพิ่มเข้าไปใน list และแสดง list ที่เรากรอกไปทั้งหมดและข้อมูลที่กรอกไปได้จะต้องเป็น string เท่านั้น number และ empty string หรือ string number

ตัวอย่าง

"junior" ✅

"junior01" ❌

213123 ❌

"" ❌

## Goals

- เข้าใจการเขียนพื้นฐาน javascript, typescript, css, html

## Installation

```bash
yarn
```

## Run Project

```bash
yarn dev
```

## Note

1. เขียน logic ใน ไฟล์ index.ts โดยใช้คำสั่ง `yarn build`(เพื่อ compile code)

2. เขียน css ใน `style.css`

3. html เขียนใน `index.html` หรือจะเขียนใน typescript ก็ได้ (something.innerHtml)
